//
//  WeatherViewControllerTests.m
//  WestpacSample
//
//  Created by Tony Thomas on 11/04/16.
//  Copyright © 2016 Tony Thomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WeatherViewController.h"


@interface WeatherViewControllerTests : XCTestCase

@property (nonatomic, strong)  WeatherViewController* vewController;
@end

@implementation WeatherViewControllerTests

- (void)setUp {
    [super setUp];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.vewController = [storyboard instantiateInitialViewController];
    [self.vewController performSelectorOnMainThread:@selector(loadView) withObject:nil waitUntilDone:YES];

    // Put setup code here. This method is called before the invocation of each test method in the class.
}
- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testViewControllerCreated{
    
    XCTAssertNotNil(self.vewController,  @"ViewController cannot be nil");
}
- (void)testParentViewHasTableViewSubview
{
    NSArray *subviews = self.vewController.view.subviews;
    XCTAssertTrue([subviews containsObject:self.vewController.tableView], @"View does not have a table subview");
}


#pragma mark - UITableView tests


- (void)testThatTableViewHasDataSource
{
    XCTAssertNotNil(self.vewController.tableView.dataSource, @"TableView datasource cannot be nil");
}

- (void)testThatTableViewHasDelegate
{
    XCTAssertNotNil(self.vewController.tableView.delegate, @"Tableview delegate cannot be nil");
}

- (void)testWeatherTableViewIsConnectedToDelegate
{
    XCTAssertNotNil(self.vewController.tableView.delegate, @"Table delegate cannot be nil");
}




@end
